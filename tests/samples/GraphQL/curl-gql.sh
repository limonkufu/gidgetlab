#! /bin/sh
curl "https://gitlab.com/api/graphql" --header "Authorization: Bearer $GRAPHQL_TOKEN" \
     --header "Content-Type: application/json" --request POST \
     --data " \
    { \
       \"query\": \" \
         query (\$number_of_issues: Int!) { \
           project(fullPath: \\\"gitlab-org/graphql-sandbox\\\") { \
             name \
             issues(first: \$number_of_issues) { \
               nodes { \
                 title \
               } \
             } \
           } \
         } \", \
      \"variables\": { \
        \"number_of_issues\": 3
      } \
    } \
    "
